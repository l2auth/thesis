# Preventing Supply Chain Insecurity by Authentication on Layer 2


## Build requirements

- A (pdf)LaTeX installation (including the most commonly used packages)
- BibTeX (for bibliography management)

"Optional":
- GNU Make
- latexmk
- `inkscape` (to convert `*.ai` ZHAW logos to PDF)

## Building

To build the PDF version of this thesis:
```sh
make pdf  # creates thesis.pdf
```

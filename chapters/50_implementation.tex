% -*- mode: latex; coding: utf-8; TeX-master: ../thesis -*-
% !TEX TS-program = pdflatexmk
% !TEX encoding = UTF-8 Unicode
% !TEX root = ../thesis.tex

As part of this thesis we built a working proof of concept (PoC) implementation
for the GNU/Linux operating system. It consists of two distinct
components:

\begin{itemize}
  \item{
    an out-of-tree Linux kernel module which
    contains the implementation of the \emph{L2auth} protocol.
  }
  \item{
    an extension module to the \emph{iproute2} software package which
    is used to set up the virtual network interface for L2auth (incl.
    the pre-shared key.)
  }
\end{itemize}

This chapter has two objectives.
On one hand, it describes what has been implemented and how this has been done.
On the other hand, it serves as the documentation we didn't have for future work
trying to achieve a similar goal.

\textbf{Please note:} In this chapter we will include code snippets for
illustrative purposes. They are shortened and might not work correctly as they
are shown. For a working example please refer to the real implementaton which
can be found at: \\
\url{https://gitlab.com/l2auth/l2auth.git}


\section{Linux kernel module}
\label{sec:linux-kernel-module}

Initially we planned to modify a \gls{NIC} driver already present in the Linux
kernel. After having looked at some of the popular ones, it became clear that
modifying a multiple thousand lines long C file full of specific hardware
details is not the best idea\,\ldots{}  So we were on the lookout for a better
solution, and while looking at the way tagged VLAN handling is implemented in
the Linux kernel, we got the idea to implement our own \gls{NIC} agnostic kernel
module.

That kernel module is supposed to create a new network interface which only
authenticated frames can ``traverse''.  It should also hook into the packet
receiving process of the Linux kernel and redirect packets received on
the real network interface to the virtual interface if they are authenticated
correctly.


\subsection{Simple pass-through net driver}
\label{sec:register-net-driver}

The first step was to get our kernel module set up so that we could create
virtual network interfaces which do nothing else than forwarding network traffic
between the real (underlying) and virtual (upper) interface.

To do that, it is important to first understand how network interfaces are
configured in the Linux kernel, because we will need to use this mechanism to
create our upper interface.

\subsubsection{Configuring network interfaces}
\label{sec:config-net-ifaces}

Linux network interfaces are configured through a special type of socket, the
so called \gls{netlink-socket}.  It is an interface from the user space to the
Linux kernel which is used to configure network interfaces, but not limited to
this purpose.

There are different netlink families available (see \verb|man 7 netlink| for
more information.)
To configure network interfaces, the \textsl{rtnetlink} (the routing netlink
family) is used:

\begin{quote}
\makeatletter\renewcommand{\verbatim@font}{\ttfamily\small}\makeatother
\begin{verbatim}
NETLINK_ROUTE
    Receives routing and link updates and may be used to modify the
    routing tables (both IPv4 and IPv6), IP addresses, link parameters,
    neighbor setups, queueing disciplines, traffic classes and
    packet classifiers (see rtnetlink(7)).
\end{verbatim}
\signed{netlink(7)}
\end{quote}


\subsubsection{Building a kernel module with an rtnetlink interface}
\label{sec:kmod-rtnl}

For a Linux kernel module to provide an rtnetlink (sometimes
abbreviated to \textsl{rtnl}) interface, the kernel module must register itself
with the rtnetlink infrastructure in the kernel.

This is done by preparing a \lstinline[language=C]{struct rtnl_link_ops} with
function pointers and some metadata, as shown in
Listing~\ref{lst:rtnl-link-init}.
When the kernel module gets loaded, the struct has to be passed to
\lstinline[language=C]{rtnl_link_register()} to inform the kernel about the new
operations.
\begin{lstlisting}[language=C,caption={Registering L2auth with rtnetlink},%
  label={lst:rtnl-link-init}]
static struct rtnl_link_ops l2auth_link_ops __read_mostly = {
  .kind = "l2auth",
  /* amount of memory the kernel should allocate for private device data */
  .priv_size = sizeof(struct l2auth_dev),

  /* function to define the structure and functionality of a newly created link */
  .setup = l2auth_setup,
  /* function to configure a new link on a netdev created with .setup */
  .newlink = l2auth_newlink,
  /* function to alter properties of an existing link */
  .changelink = l2auth_changelink,
  /* function to delete a link */
  .dellink = l2auth_dellink,

  /* ... */
};

static int __init l2auth_init(void)
{
  return rtnl_link_register(&l2auth_link_ops);
}

static void __exit l2auth_exit(void)
{
  rtnl_link_unregister(&l2auth_link_ops);
}
\end{lstlisting}

After we have the module registered with rtnetlink and new network interfaces can
be created, we ought to look at the functions required to prepare a new
network interface to be functional.

The first function that is called after the kernel has allocated a new netdev is
\lstinline[language=C]{l2auth_setup()}.  Its job is to populate all the fields
and properties of the interface with their respective values. It also configures
the operations that can be executed on the netdev itself by preparing an appropriate
\lstinline[language=C]!struct net_device_ops! (cf.~Listing~\ref{lst:netdev-ops}).

\begin{lstlisting}[language=C,label={lst:netdev-ops},caption={Setting up a new net device and declaring operations}]
static const struct device_type l2auth_type = {
  .name = "l2auth",
};

static const struct net_device_ops l2auth_netdev_ops = {
  /* function to create a new device. This function is triggered by the command:
   * ip link add link [...] type l2auth */
  .ndo_init = l2auth_dev_init,
  /* function to remove an existing device. This function is triggered by the command:
   * ip link del link [...] */
  .ndo_uninit = l2auth_dev_uninit,
  /* function to enable ("up") a device. This function is triggered by the command:
   * ip link set [...] up */
  .ndo_open = l2auth_dev_open,
  /* function to disable ("down") a device. This function is triggered by the command:
   * ip link set [...] down */
  .ndo_stop = l2auth_dev_stop,

  /* function to transmit a packet on this interface */
  .ndo_start_xmit = l2auth_start_xmit,

  .ndo_change_mtu = l2auth_change_mtu,
  .ndo_set_mac_address = l2auth_set_mac_address,

  /* ... */
};

static void l2auth_setup(struct net_device *dev)
{
  /* Initialise a basic ethernet device */
  ether_setup(dev);

  /* Set the type of the device */
  SET_NETDEV_DEVTYPE(dev, &l2auth_type);

  /* Declare the operations supported on this device */
  dev->netdev_ops = &l2auth_netdev_ops;
}
\end{lstlisting}

As you might have noticed in Listing~\ref{lst:netdev-ops}, there is no
function in \lstinline[language=C]{l2auth_netdev_ops} for receiving frames.
This is because in the Linux kernel the processing of received frames
is not directly handled by the network driver itself.
We will be looking at how this works later in the
\textsl{\nameref{sec:kernel-data-flow}} section.

After \lstinline[language=C]{l2auth_setup()} has completed, another function
\lstinline[language=C]{l2auth_newlink()} (cf.~Listing~\ref{lst:newlink-func})
will be executed.  The \verb|newlink| function will prepare the private device
data based on the options the user has passed in (e.g.\ the underlying interface
or the pre-shared key), establish the link between the underlying interface and
the new virtual interface, and finally register the virtual interface with the
kernel.

\begin{lstlisting}[language=C,label={lst:newlink-func},caption={Configuring a newly created net device}]
static int l2auth_newlink(struct net *net, struct net_device *dev,
                          struct nlattr *tb[], struct nlattr *data[],
                          struct netlink_ext_ack *extack)
{
  struct l2auth_dev *l2auth = l2auth_priv(dev);
  struct net_device *real_dev;
  rx_handler_func_t *rx_handler;
  int err;

  /* Check if the underlying device is present and set real_dev */
  if (!tb[IFLA_LINK])
    return -ENODEV;

  if (!(real_dev = __dev_get_by_index(net, nla_get_u32(tb[IFLA_LINK]))))
    return -ENODEV;

  l2auth->real_dev = real_dev;

  /* Configure the MTU by taking real_dev's MTU and subtracting the L2auth overhead */
  dev->mtu = (real_dev->mtu - L2AUTH_EXTRA_LEN);

  /* Configure the receive handler */
  rx_handler = rtnl_dereference(real_dev->rx_handler);
  if (rx_handler && rx_handler != l2auth_rx_handler) {
    /* the underlying interface already has a rx_handler registered -> error */
    return -EBUSY;
  }

  register_netdevice(dev);
  dev_hold(real_dev);
  netdev_upper_dev_link(real_dev, dev, extack);

  /* Prepare structures for crypto */
  l2auth_crypto_init(dev);
  l2auth_changelink_common(dev, data);

  /* Register the rx_handlers, eventually */
  l2auth_register_rx_handlers(real_dev, dev);

  return 0;
}
\end{lstlisting}

After \verb|l2auth_newlink()| has completed, \verb|l2auth_dev_init()| is run.
This function takes the newly created interface and configures the supported
features, defines the needed head- and tailroom (needed for protocol
extensions), and finally sets the MAC address of the interface.

\pagebreak
With all of these functions that have been registered, it can be difficult to
keep a good overview of which function gets called at which point.
We visualised all the functions and in what order they are executed in
Figure~\ref{fig:call-diagram}.

\begin{figure}[H]
  \centering
  \includegraphics[width=\linewidth]{call_diagram}
  \caption{Diagram showing the most important functions}
  \label{fig:call-diagram}
\end{figure}

\pagebreak
\subsection{Data flow}
\label{sec:kernel-data-flow}

As can be seen in Figure~\ref{fig:call-diagram}, there are three functions that
we haven't looked at yet: \verb|l2auth_start_xmit()|,
\verb|l2auth_rx_handler_under()|, and \verb|l2auth_rx_handler()|.
These functions are used to send and receive network frames.

Please refer to Figure~\ref{fig:data-flow} for an overwiew of what these functions
are used for and where they are situated.
% We registered callback function that are executed as shown in the figure~\ref{fig:data-flow}.
% These function calls have a green highlight. The highlighted arrows is the data flow,
% that are defined while executing the callback itself.

\begin{figure}[H]
\centering
\includegraphics[width=.7\linewidth]{data-flow}
\caption{Linux packet processing data flow}
\label{fig:data-flow}
\end{figure}

In the Linux kernel, packets are handled in so called \verb|sk_buff|
structures. Their name is usually abbreviated as \verb|skb|.  The skb contains a
buffer which contains the whole network packet, and some extra space (head- and
tailroom) to be used by lower layers to add the data they need later on.

\subsubsection{Transmission}

Transmission of a frame is fairly straightforward, because we can assume that
the data we receive is legit.
Sending frames is handled by \verb|l2auth_start_xmit()| in which we have the
chance to alter the datagram before sending it off.
We use this to authenticate the frame and redirect it to the underlying Ethernet
device, as we don't have any means to putting it on the wire ourselves.

\vfil\pagebreak
A simple \verb|start_xmit()| function could look like this:
%
\begin{lstlisting}[language=C,caption={Start the transmission of a frame}]
static netdev_tx_t l2auth_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
  /* skb contains the full, ready to send Ethernet frame.
   * dev is the local interface the frame was posted to. */

  /* Authenticate frame (alters the EtherType and appends the HMAC) */
  l2auth_authenticate_skb(skb, dev);

  /* Redirect frame to underlying interface */
  skb->dev = l2auth_priv(dev)->real_dev;

  /* Queue the frame for transmission */
  dev_queue_xmit(skb);

  return NETDEV_TX_OK;
}
\end{lstlisting}
\vspace{-\baselineskip}

\subsubsection{Reception}

Receiving a frame is a bit more sophisticated.
While a driver can register a receive handler (\verb|rx_handler|), unlike what
one could reasonably expect after having seen the \verb|xmit| function, it does
not ``receive'' a datagram, technically speaking.
The \verb|rx_handler| behaves more like a hook function in the sense that it is
neither responsible for reading the data from the wire, nor making sure that the
received frame gets further processed.
%
The \verb|rx_handler| sits in between. It gets called when the datagram has
already been read from the wire and the \verb|rx_handler| hook function now has
the chance to give instructions (by returning different values) to the kernel on
how to proceed.

An \verb|rx_handler| can have four different return values:
\begin{lstlisting}[language=C,caption={Possible return values of an rx\_handler}]
/* include/linux/netdevice.h */
/*
 * enum rx_handler_result - Possible return values for rx_handlers.
 * @RX_HANDLER_CONSUMED: skb was consumed by rx_handler, do not process it
 * further. [i.e. drop the frame]
 * @RX_HANDLER_ANOTHER: Do another round in receive path. This is indicated in
 * case skb->dev was changed by rx_handler.
 * @RX_HANDLER_EXACT: Force exact delivery, no wildcard.
 * @RX_HANDLER_PASS: Do nothing, pass the skb as if no rx_handler was called.
 *
 * [...]
 */

enum rx_handler_result {
  RX_HANDLER_CONSUMED,
  RX_HANDLER_ANOTHER,
  RX_HANDLER_EXACT,
  RX_HANDLER_PASS,
};
typedef enum rx_handler_result rx_handler_result_t;
\end{lstlisting}

After checks have been passed successfully the \verb|rx_handler| returns
\verb|RX_HANDLER_PASS| and the kernel will forward the frame to the respective
layer 3 protocol handler based on the frame's \gls{ethertype}.

However, here is one final challenge to solve:
Since frames are received by the underlying interface, it would seem intuitive to
register an \verb|rx_handler| on the underlying device, check the authentication
and pass on the frame.
Unfortunately, some protocols, like \gls{ARP}, don't accept responses on a
different interface than the request has been sent from.
In the case of \gls{ARP} such a response resembles a \textsl{Gratuitous ARP}
response, which by default is ignored by the Linux kernel.

To work around this, we register two \verb|rx_handler|s: one on the underlying
interface, which does nothing but redirecting the frame to the upper interface,
and one on the upper interface, which checks the authentication and passes on
the frame if it is valid (otherwise it gets dropped).

\vfil\pagebreak% HACK
Such a ``two handler'' setup could look like this:
\begin{lstlisting}[language=C,caption={Redirecting and receiving frames on the upper interface}]
static rx_handler_result_t l2auth_rx_handler_under(struct sk_buff **pskb)
{
  struct sk_buff *skb = *pskb;
  struct list_head *dev_upper_list;
  struct net_device *upper;

  /* Get the list of all registered upper devices */
  dev_upper_list = &(skb->dev->adj_list.upper);
  /* We assume there is only one upper device */
  upper = netdev_upper_get_next_dev_rcu(skb->dev, &dev_upper_list);

  if (upper) {
    /* Redirect this frame to the upper interface */
    skb->dev = upper;

    /* Tell the caller that we redirected this frame to another interface.
       The kernel will then look up the rx_handler of the new interface and call
       it (= do *another* round). */
    return RX_HANDLER_ANOTHER;
  }

  /* We should not get here! If this handler is registered, but no upper device
     is present, we assume that this handler has not been properly unregistered
     after deleting an L2auth interface. We assume this is okay and pass on the frame */
  return RX_HANDLER_PASS;
}

static rx_handler_result_t l2auth_rx_handler(struct sk_buff **pskb)
{
  /* The buffer to the datagram */
  struct sk_buff *skb = *pskb;

  /* Check frame's EtherType */
  if (ntohs(l2auth_skb_get_header(skb)->eth.h_proto) != ETH_P_L2AUTH)
    goto drop;

  if (l2auth_check_auth_skb(skb)) {
    /* HMAC of received frame is invalid */
    goto drop;
  }
  /* else: the frame is fine! */

  /* Unpack the frame (replace the EtherType with the real one and remove the HMAC) */
  l2auth_unpack_frame(skb);

  /* The frame was an L2auth frame with correct authentication: pass! */
  return RX_HANDLER_PASS;

drop:
  /* Drop the frame! This is done by telling the kernel that we took care of the frame */
  return RX_HANDLER_CONSUMED;
}
\end{lstlisting}

At this point we have all that's needed for creating an upper interface and
processing frames as required by the L2auth protocol.

\subsection{HMAC calculation}

Next, we have a closer look at the Linux kernel's cryptographic API
and how it can be used to generate the \glspl{HMAC} we need for authentication.

\subsubsection{Introduction to the Linux cryptographic API}
\label{sec:kernel-crypto-api-intro}

The Linux kernel includes its own built-in crypto library.
It is easy to use, fast (including support for hardware acceleration), and is
more or less well documented.

The architecture of the kernel's crypto library is as follows:
\begin{quote}
\makeatletter\renewcommand{\verbatim@font}{\ttfamily\small}\makeatother
\begin{verbatim}
At the lowest level are algorithms, which register dynamically with the
API.

'Transforms' are user-instantiated objects, which maintain state, handle
all of the implementation logic [...] and provide an abstraction to the
underlying algorithms.  However, at the user level they are very simple.

Conceptually, the API layering looks like this:

  [transform api]  (user interface)
  [transform ops]  (per-type logic glue e.g. cipher.c, compress.c)
  [algorithm api]  (for registering algorithms)

The idea is to make the user interface and algorithm registration API
very simple, while hiding the core logic from both.
\end{verbatim}
\signed{Scatterlist Cryptographic API\cite{linux-crypto-api-intro}}
\end{quote}

\vfil\pagebreak
\subsubsection{HMAC calculation using the Linux cryptographic API}
\label{sec:using-kernel-crypto}

To calculate an HMAC we must first allocate a so called transform, like this:

\begin{lstlisting}[language=C,caption={Initialising an SHASH transform for HMAC-SHA256},label={lst:tfm-init}]
struct crypto_shash *tfm = crypto_alloc_shash("hmac(sha256)", 0, 0);
__u8 key[keylen] = /* ... */;
crypto_shash_setkey(tfm, key, keylen);
\end{lstlisting}

Then there is one hurdle left to overcome. The crypto modules do not
allocate memory for their operation on their own. We need to provide them
with a so called ``desc'' buffer.

To calculate the HMAC of an input in \verb!buf! with length \verb!len! and store
it in \verb|res|, the following code can be used:
\begin{lstlisting}[language=C,caption={Performing HMAC calculation using the transform initialised in Listing~\ref{lst:tfm-init}},label={lst:simple-hmac}]
/* Allocate memory for the sdesc struct */
struct shash_desc *sdesc = kmalloc(sizeof(struct shash_desc) + crypto_hash_descsize(tfm), GFP_KERNEL);

sdesc->tfm = tfm;
sdesc->flags = 0x0;

/* Perform the HMAC operation */
crypto_shash_digest(sdesc, buf /* the input */, len, res /* the resulting HMAC */);

kfree(sdesc);
\end{lstlisting}
\vspace{-\baselineskip}

% We assume that the allocation of memory is left to the caller for performance
% reasons.  We could imagine that the idea is to leverage spatial locality between
% the data and the \verb|sdesc| buffer.

Running through \verb|kmalloc| for each packet we need to authenticate
or verify is quite a bit of overhead. Therefore, we replaced the call to \verb|kmalloc| in
Listing~\ref{lst:simple-hmac} with the
\lstinline[language=C]|SHASH_DESC_ON_STACK| macro which is much faster because
it allocates space on the stack basically for free.

\begin{lstlisting}[language=C,caption={SHASH\_DESC\_ON\_STACK macro evolution}]
  /* include/crypto/hash.h:942 (Linux 4.19.x) */
  #define SHASH_DESC_ON_STACK(shash, ctx)                   \
      char __##shash##_desc[sizeof(struct shash_desc) +     \
          crypto_shash_descsize(ctx)] CRYPTO_MINALIGN_ATTR; \
      struct shash_desc *shash = (struct shash_desc *)__##shash##_desc

  /* include/crypto/hash.h:158 (Linux 4.20-rc1) */
  #define HASH_MAX_DESCSIZE  360
  #define SHASH_DESC_ON_STACK(shash, ctx)                   \
      char __##shash##_desc[sizeof(struct shash_desc) +     \
          HASH_MAX_DESCSIZE] CRYPTO_MINALIGN_ATTR; \
      struct shash_desc *shash = (struct shash_desc *)__##shash##_desc
\end{lstlisting}

NB: This macro uses variable-length arrays (VLA) until Linux 4.19, but subsequently
has been rewritten to use a statically defined upper-bound.
% https://lkml.org/lkml/2018/3/7/621

Finally, a simple proof of concept code for HMAC calculation of a frame in the
Linux kernel looks like this:
\begin{lstlisting}[language=C,caption={Proof of Concept: HMAC calculation of an sk\_buff using Linux cryptographic API}]
int poc_hmac(const struct sk_buff *skb, size_t len, const __u8 *key, size_t keylen,
             unsigned char *res)
{
  struct crypto_shash *tfm = crypto_alloc_shash("hmac(sha256)", 0, 0);
  SHASH_DESC_ON_STACK(sdesc, tfm);
  void *skb_buf = skb_mac_header(skb);
  int ret;

  sdesc->tfm = tfm;
  sdesc->flags = 0x0;

  /* set the key */
  if ((ret = crypto_shash_setkey(tfm, key, keylen)))
    return ret;

  /* perform HMAC calculation of skb_buf and write result to res */
  return crypto_shash_digest(sdesc, skb_buf, len, res);
}
\end{lstlisting}

So far, we are able to calculate the \gls{HMAC} of an arbitrary frame
using a pre-defined key, but there is no means for the user to set the key, yet.
What we need is a way to pass the key from user space into kernel space.


\section{Interface configuration using netlink and iproute2}
\label{sec:iface-configuration}

Similarly to how we create new network interfaces, we can also use the
\gls{netlink-socket} to pass arbitrary configuration options to a network
interface.

Identification of configuration options sent through the
\gls{netlink-socket} happens by the use of attribute codes. These codes are
declared as IFLA attributes\footnote{IFLA is the
  acronym of ``InterFace Link Attribute.'' (we are aware that calling them
  ``IFLA attributes'' is a pleonasm, but we decided to keep calling them this
  way because all the constants in the code start with \texttt{IFLA\_})},
which are integers referring to an \lstinline[language=C]{enum} in the kernel
code and thus need to be declared both in the kernel and the respective user
space component.

\subsection{The kernel side}

We start in the kernel by updating the \texttt{l2auth} module.

\subsubsection{Defining IFLA attributes}

Since there are no pre-existent IFLA attributes in rtnetlink
that could be used for passing a pre-shared key, we extended it with custom
IFLA attributes:

\begin{lstlisting}[language=C,caption={Definition of IFLA attributes},label={lst:ifla-def}]
enum {
  IFLA_L2AUTH_UNSPEC,
  IFLA_L2AUTH_KEY,
  IFLA_L2AUTH_BACKUP_KEY,
  __IFLA_L2AUTH_MAX,
};

#define IFLA_L2AUTH_MAX (__IFLA_L2AUTH_MAX - 1)
\end{lstlisting}
\vspace{-\baselineskip}
% As can be seen in Listing~\ref{lst:ifla-def}, the definition of the options is
% only an \lstinline[language=C]{enum}, thus the kernel does not know the names
% what we use in user space.

The numbering of the \lstinline[language=C]{enum} starts at 0 for each link
type but 0 must not be used for an actual attribute.
For this reason it is a convention in the kernel to add an \verb!_UNSPEC!
attribute first.

For in-tree drivers the definition of the attributes is located in the header
file \\ \verb!include/uapi/linux/if_link.h!.
But since we build an out-of-tree kernel module, we created a separate header
file in our own source tree containing nothing but the \verb!IFLA_L2AUTH_!
attributes.


Having the rtnetlink attribute codes defined, we created a
\lstinline[language=C]{struct nla_policy}, in which the types and (optionally)
length limitations of the attributes are set.

Additionally, we extended the \verb|l2auth_link_ops| structure with more
metadata:

\begin{lstlisting}[language=C,caption={Configuring NLA policy}]
static const struct nla_policy l2auth_nla_policy[IFLA_L2AUTH_MAX + 1] = {
  [IFLA_L2AUTH_KEY] = { .type = NLA_BINARY },
  [IFLA_L2AUTH_BACKUP_KEY] = { .type = NLA_BINARY },
};

static struct rtnl_link_ops l2auth_link_ops __read_mostly = {
  /* ... */

  .policy = l2auth_nla_policy,      /* list of all possible config options */
  .maxtype = IFLA_L2AUTH_MAX,       /* highest defined attribute code */
  .get_size = l2auth_nla_get_size,  /* function that returns the max. size of config options */
};
\end{lstlisting}


\subsubsection{Processing IFLA attributes in kernel space}

Processing IFLA attributes happens in the functions called
\verb|l2auth_newlink()| and \verb|l2auth_changelink()|.
Both functions take \verb|tb| and \verb|data| parameters, which are are lists
containing the configuration values.
%
\verb|tb| contains all the commonly used attributes for interfaces, e.g.\ the
parent device.
\verb|data| contains all the device specific attributes, in our case the keys.

To access the data of a configuration option we simply access the \verb|data|
array at the respective index.  The index is determined by the IFLA attribute
code described above.
%
But we must be careful when doing this because in case the user did not specify
the attribute in question, the value at that index is \lstinline[language=C]{NULL}.

\begin{lstlisting}[language=C,caption={Processing link options in newlink function}]
static int l2auth_newlink(struct net *net, struct net_device *dev, struct nlattr *tb[],
                          struct nlattr *data[], struct netlink_ext_ack *extack)
{
  /* ... */

  if (data[IFLA_L2AUTH_KEY]) {
    const __u8 *key = nla_data(data[IFLA_L2AUTH_KEY]);
    int len = nla_len(data[IFLA_L2AUTH_KEY]);

    /* Check length of key */
    if (len > L2AUTH_KEY_LEN || len < L2AUTH_KEY_LEN_MIN)
      return -EINVAL;

    if ((ret = crypto_shash_setkey(l2auth->tfm, key, len)))
      return ret;

    l2auth->has_key = 1;
  }

  /* ... */
}
\end{lstlisting}


\subsection{The user space}

For the user space component we decided to extend
\textsl{\href{https://wiki.linuxfoundation.org/networking/iproute2}{iproute2}}
instead of implementing our own solution.
The development of iproute2 is closely tied to the Linux kernel, and it
is getting increasingly more popular in the Linux community, slowly replacing
other tools for configuring network interfaces\footnote{
  Some distributions even stopped to install the old tooling by default.}.

There are many benefits in extending iproute2. It can be easily extended using
dynamic libraries which allows for very easy distribution as an unofficial
extension in an early phase while users can continue using their well-known tool
for configuring all network interfaces.

\subsubsection{iproute2 extension mechanism}
\label{sec:iproute-ext-mechanism}

Unfortunately, we could not find any kind of documentation or literature on how
to extend iproute2.  To overcome this we studied the code intensively
and built up our own documentation.
Please also note that some embedded systems use busybox to
provide their \verb|ip| command which does \emph{not} support extension.

The extension mechanism of iproute2 seems to be limited to adding new
attributes that can be used for configuring a link (\verb|ip link add| or
\verb|ip link set|) and extracting detailed statistics from a specific link
type.

To get iproute2 to load the dynamic library it must be placed at a
pre-defined location in the filesystem, namely \verb|${LIBDIR}/ip/link_${type}.so|.
\verb|${LIBDIR}| is defined by a configuration parameter at compile-time of
iproute2. Usually it is \verb|/usr/lib|, but some distributions
overwrite this option\footnote{examples are Alpine/Adélie/Gentoo which use
  \texttt{/lib} or RedHat derivatives which use \texttt{/usr/lib64} on
  \SI{64}{\bit} installations. To determine the path on your system, this
  command can be used:\\
  {\lstinline[language=sh,basicstyle={\footnotesize\ttfamily}]{strings -n 10 "$(command -v ip)" | grep 'link_\%s.so'}}}.
\verb|${type}| is whatever is passed to the ``type'' argument on the command
line.

There is a small limitation in the loading mechanism of iproute2 insofar
that the user must always specify \verb|type l2auth| for the extension to be
loaded.

For iproute2 to know which functions it has to call, they are declared in a
\lstinline[language=C]{struct link_util} called after the link type:

\begin{lstlisting}[language=C,caption={Declaring the custom functions for an iproute2 link extension}]
struct link_util l2auth_link_util = {
  /* name of the link type */
  .id = "l2auth",
  /* highest defined attribute code */
  .maxattr = IFLA_L2AUTH_MAX,
  /* function to parse the command line */
  .parse_opt = l2auth_parse_opt,
  /* function to display help */
  .print_help = l2auth_print_help,
};
\end{lstlisting}
\vfil

\subsubsection{Passing custom netlink attributes to kernel space}

To pass custom netlink attributes to kernel space, we don't need to reinvent the
wheel.  iproute2 already supports this feature, so all we need to do is
to process the options that the user has given in the command line and bring
them into the appropriate form such that iproute2 can process them further.

For this purpose we implement a \verb|parse_opt()| function in which we iterate
over \verb|argv| and add the arguments we processed to
\lstinline[language=C]|struct nlmsghdr *n|.
iproute2 will then generate the netlink message according to what we have
written into the \verb|n| parameter


In practice, the command line argument processing could look like this:

\begin{lstlisting}[language=C,caption={iproute2 argument processing}]
static void addattr_key(char **argv, int ifla, struct nlmsghdr *n)
{
  /* 4 extra cells to determine if the given key is longer than KEY_LEN_MAX */
  __u8 key[KEY_LEN_MAX + 4];
  int len = 0;

  if (!hexstring_a2n(*argv, key, KEY_LEN_MAX + 4, &len))
    invarg("expected SECRET to follow 'key'", "SECRET");
  if (len < KEY_LEN_MIN)
    invarg("The given key is too short!", "SECRET");
  if (len > KEY_LEN_MAX)
    invarg("The given key is too long!", "SECRET");

  /* add attribute to n */
  addattr_l(n, L2AUTH_BUFLEN, ifla, key, len);
}

static int l2auth_parse_opt(struct link_util *link_util, int argc, char **argv,
                            struct nlmsghdr *n)
{
  while (argc > 0) {
    if (matches(*argv, "key") == 0) {
      NEXT_ARG();
      addattr_key(argv, IFLA_L2AUTH_KEY, n);
    } else if (matches(*argv, "backup") == 0) {
      NEXT_ARG();
      addattr_key(argv, IFLA_L2AUTH_BACKUP_KEY, n);
    } else {
      invarg("unknown command", *argv);
    }
    NEXT_ARG_FWD();
  }

  return 0;
}
\end{lstlisting}


\section{Potential improvements}
\label{sec:impl-extensions}

\subsection{Protection against an attacker with memory access}
\label{sec:protection-memory-access}

Even though we assume that an attacker does \emph{not} have access to main
memory, it would be conceivable that a very advanced attacker does.

To prevent \gls{data-exfiltration} in such a case we devised different approaches.
Unfortunately, none of them can prevent exfiltration for sure.

\paragraph{Random relocation of the key in memory}

is a very straightforward solution to the problem. The code runs a timer that
fires in random intervals and when it fires the key is copied to a new random
memory location, the pointer in the private device data is adjusted, and the old
memory location is poisoned (i.e.\ overwritten with random data.)

This technique would add little overhead to the code but certainly slow an
attacker down because he cannot be sure to having seen the key after scanning
the address space once.
It will not provide guaranteed protection, though.
If the attacker is able to locate the device's private data structure, which
will always contain a pointer to the key, he can simply dereference the pointer
to get to the key. Unfortunately, the kernel does not provide any mechanism to
relocate the device's private data structure.


\paragraph{Using a hardware device for key storage}

Another possible approach would be to store the pre-shared key on a separate
piece of hardware, e.g.\ a smart card or a dongle.  This would allow deletion of
the key from memory after it has been used. However, this would have a performance impact
as the key would need to be loaded from the external device for each Ethernet
frame received or sent.

On most systems there is constant network activity which would result in the key
being in main memory most of the time anyway.

\paragraph{Offloading hashing to a hardware device}

A slight improvement to the second solution could be to offload the HMAC
calculation to a separate hardware device, e.g.\ a TPM chip or a very fast smart
card.
In this case the key would never have to be loaded into main memory.
While this would certainly prevent an attacker from reading the key from main
memory, this opens up a new possible attack vector.
The attacker could simply ask the hardware device to authenticate his
illegitimate frames.
To resolve this issue, the operating system would need to authenticate itself to
the hardware device to have frames authenticated which it would need to do using
a key stored in main memory.

\paragraph{Custom HMAC post processing}

After evaluating the three previous approaches, we had to realise that none of
them can provide full protection against an attacker with access to main memory.
What it boils down to is that we only have one way to differentiate between an
attacker and the legitimate operating system. The operating system knows a
secret that the attacker doesn't.  Unfortunately, there are no real alternatives
to storing such secrets in the system's main memory.  If an attacker can read
all of the memory, he can always read the keys.

This gave us the idea to try something radically different. One way to make it
much more difficult for an attacker to figure out a secret is to encode it in a
way that is more complex to understand, like in code.  To do this, we extended
the kernel module with an optional functionality to post-process the calculated
HMAC using arbitrary C code that can be injected at compile-time.

While this does make a specific build of L2auth incompatible with other builds,
it makes a generic attack targeting multiple victims infeasible.  A small caveat
to note: this method can only offer protection if the attacker is incapable of
executing the code.
\\

To conclude: there are different approaches to protect against an attacker with
access to main memory, but when you think them through, they all have their
limitations.  The first three approaches, which preserve interoperability, can
only add more layers of indirection but none of them can provide guaranteed
protection from such an attacker.  If interoperability can be sacrificed, HMAC
post-processing can be an option to consider.

The decision which solution to choose has to be taken on a case-by-case basis.

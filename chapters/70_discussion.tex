% -*- mode: latex; coding: utf-8; TeX-master: ../thesis -*-
% !TEX TS-program = pdflatexmk
% !TEX encoding = UTF-8 Unicode
% !TEX root = ../thesis.tex

% -- Bespricht die erzielten Ergebnisse bezüglich ihrer Erwartbarkeit,
%    Aussagekraft und Relevanz
% -- Interpretation und Validierung der Resultate
% -- Rückblick auf Aufgabenstellung, erreicht bzw. nicht erreicht
% -- Legt dar, wie an die Resultate (konkret vom Industriepartner oder
%    weiteren Forschungsarbeiten; allgemein) angeschlossen werden kann; legt
%    dar, welche Chancen die Resultate bieten

During this thesis we have designed an extension protocol on the \gls{OSI} MAC
layer which allows to prevent any unauthenticated Ethernet frames from leaving
the broadcast domain or reaching the receiver's operating system's networking
stack. As proof of concept we implemented the protocol in the Linux
kernel in the form of an out-of-tree Linux kernel module.

Both the design and the implementation have been verified in the field and both
yield the requested security guarantees and performance results outlined in the
\textsl{\nameref{sec:objective}}.
%
The \gls{HMAC}-SHA256 we applied for authenticating datagrams provides the
required 128\,bits of security, and exfiltration of data can be prevented in a
correctly configured setup (i.e.\ L2auth is enabled on all clients and
the gateway.)
%
Performance-wise, we measured a reduction of \SI{26}{\percent} in the maximum
throughput on old laptop hardware (a 2010 MacBook Pro).  Taking into account the
age of the hardware used, we consider the results to be good.
The maximum throughput could be increased by using newer hardware with a
\gls{SHA256} accelerator.


\section{Advantages over MACsec}
\label{sec:advantages-over-macsec}

Of course, when a new solution is presented, the question ``why and how is it
better than previous solutions?'' comes up.  To answer this question we would
like to elaborate a bit on the advantages L2auth can provide over the
most prominent alternative, MACsec.

\begin{itemize}
  \item{
    L2auth follows the KISS principle\footnote{KISS is an acronym
      of ``keep it simple, stupid.''}. While designing L2auth we only
    included what is really necessary.

    MACsec, on the other hand, suffers from feature creep.
    There are too many configuration options: encryption can be
    enabled/disabled, validation of frames can optionally be disabled, replay
    protection is configurable, and the key agreement protocol (MKA) is a
    science in itself.

    Or in other words:
    \begin{quote}
      \verb|options == not(interoperable) == not(secure) == not(used)|
      \signed{Emil Isaakian\,\cite{ess-talk}}
    \end{quote}
  }
  \item{
    L2auth is easier to implement, which can be a key advantage especially
    in (lower-priced) hardware.
    The simplicity of L2auth would allow for hardware support even in
    cheaper consumer-level hardware.
  }
  \item{
    The fact that L2auth does not provide encryption allowed us to
    compromise on forward and future secrecy which in turn enabled the use of
    much simpler key management.
    Both the absence of encryption and the simpler key management facilitate
    debugging.

    Even if encryption is disabled in MACsec, there is a separate key
    pair for each pair of hosts that communicate with each other.
    This makes finding errors much more difficult, especially when dealing with
    cryptographic errors.
  }
\end{itemize}

NB: Although pursuing a different goal, MACsec can be better suited for
cloud environments as it allows to protect against a malicious cloud provider
using layer 2 encryption.

\section{Future work}
\label{sec:future-work}

Based on the discoveries, designs and implementation of this thesis, we
propose the following topics for future study:

\subsection{Support for other operating systems}
\label{sec:future-other-os}

As the time available for this thesis was limited, we could only produce a
working implementation of the L2auth protocol for the modern Linux
kernel (at least kernel version 4.15.0).  This decision was taken based on the
huge market share Linux has in the server market.

Unfortunately, only few networks are \SI{100}{\percent} Linux based.  To ease
the transition to fully L2auth protected networks for mixed
environments, implementations for other operating systems are vital for the
future of L2auth.

% \subsection{Hardware Crypto Acceleration}
% \label{sec:future-hw-crypto}

% For the initial proof of concept only the standard kernel hash function
% implementation has been used.

% A lot of systems have hardware accelerated cryptographic hashes. If these could
% be used instead of CPU only there would be a big performance improvement.

\subsection{Hardware bridge}
\label{sec:future-hw-bridge}

Although the requirements stated in the \textsl{\nameref{sec:objective}}
(i.e.\ protection from \gls{data-exfiltration}) can be met with the existing proof of
concept implementation, hardware support in network bridges (commonly called
``switches'') could further improve protection:
\begin{enumerate}
  \item{
    by preventing attackers within the same broadcast domain from
    communicating with one another.
  }
  \item{
    by limiting the range of a \gls{DoS} attack mounted by a malicious spy chip
    to the cable the compromised client is connected to.  Without bridge
    support, a \gls{DoS} attack could interfere with the whole broadcast domain.
  }
\end{enumerate}

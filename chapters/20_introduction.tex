% -*- mode: latex; coding: utf-8; TeX-master: ../thesis -*-
% !TEX TS-program = pdflatexmk
% !TEX encoding = UTF-8 Unicode
% !TEX root = ../thesis.tex

\section{Overview}
\label{sec:overview}

This thesis is organised as follows into six chapters:

The first chapter is an introduction to the subject and explains the
\hyperref[sec:objective]{objective} of this work.
In addition it introduces the terminology and concepts used. \\
The introduction is followed by an overview of
\textsl{\hyperref[ch:related-work]{related work}}. \\
In the third chapter (\textsl{\nameref{ch:protocol-design}}) we introduce the
\hyperref[sec:attacker-model]{attacker model} and give an overview of our
\hyperref[sec:finding-solutions]{different approaches}, followed
by an elaboration of our \hyperref[sec:protocol-design]{finally chosen design}. \\
Finally, we get our hands dirty in the \textsl{\nameref{ch:implementation}}
chapter.  In this chapter we \hyperref[sec:linux-kernel-module]{dive into the
  Linux kernel} and show how we integrated the protocol we devised into it.
It concludes with our approach to \hyperref[sec:iface-configuration]{configure
  the kernel module} from user space. \\
In the \textsl{\nameref{ch:results}} chapter we put our code to the test and
report on the \hyperref[sec:benchmarks]{performance metrics}. We also show that
the implementation \hyperref[sec:protection-test]{withstands common attack patterns}. \\
We conclude with the \textsl{\nameref{ch:discussion}} in which we analyse the
results, point out the \hyperref[sec:advantages-over-macsec]{advantages of our
  solution}, and propose ideas for \hyperref[sec:future-work]{future work}.

To understand the rest of this thesis, a basic understanding of the common
networking stack is required.
Should you believe that you need to brush-up on the topic, you can find
a brief refresher on the related topics in the appendix
(\textsl{\nameref{sec:networking-primer}}).


\section{Starting point}
\label{sec:starting-point}

As digitalisation is constantly increasing in all areas of our life, the amount
of data stored in data centres keeps up with the current pace of data
gathering tendencies, which in turn makes servers processing this data a more
interesting target for attackers. New ways to attack IT systems are revealed every
day and things that seemed utopian ten years ago are realistic attacks today,
which leads to an ever growing need for IT security.

An October 4, 2018 Bloomberg article entitled
\textsl{The Big Hack: How China Used a Tiny Chip to Infiltrate U.S. Companies}\,%
\cite{bloomberg-big-hack}
generated quite a stir in the security community by pointing out one of these
new threats.
In this article, the author described how ``the attack by Chinese spies reached
almost 30 U.S. companies, including Amazon and Apple, by compromising America's
technology supply chain'' using a tiny chip soldered on to a server's motherboard.
This chip allegedly ``allowed the attackers to create a stealth doorway into any
network that included the altered machines.''

Electronic components have become so small that they are barely visible to the
naked eye.
Yet they are so advanced that they can do complex calculations, have
their own memory, and can communicate with other components and computers via
USB or the network.

The network protocols that are in use today have been designed in the last
millennium and therefore do not offer adequate protection against modern
threats.  Trusted networking is set to become a vital factor in tomorrow's data
centres.  With our research we try to draw attention to this topic and point out
potential approaches to solve such issues in the future.


\section{Solutions using existing technologies}

There is much previous work on the problem of secure network communication.
Most prominently VPN solutions, like \emph{IPsec} or \emph{WireGuard}, have
been invented and can be used to protect network communication over a public
internet link.

% Note: This occurrence of internet is intentionally not capitalised, as it does
% not necessarily have to reference the Internet as a carrier medium.
However, VPN solutions have one major disadvantage: their configuration is
complex because a VPN tunnel needs both a ``physical'' network for its
communication and a ``virtual'' network for the protected network traffic.
Both of these networks need to be configured on all devices, which is
complex, error-prone, and does not scale well.
In addition, a complex firewall configuration is required to block all non-VPN
traffic on the network.


\section{Objective}
\label{sec:objective}

% -- Formuliert das Ziel der Arbeit
% -- Verweist auf die offizielle Aufgabenstellung des/der Dozierenden im Anhang
% -- (Pflichtenheft, Spezifikation)
% -- (Spezifiziert die Anforderungen an das Resultat der Arbeit)
% -- (Übersicht über die Arbeit: stellt die folgenden Teile der Arbeit kurz vor)
% -- (Angaben zum Zielpublikum: nennt das für die Arbeit vorausgesetzte Wissen)
% -- (Terminologie: Definiert die in der Arbeit verwendeten Begriffe)

The objective of this bachelor thesis is to find and implement a solution in the
Linux kernel that is able to block all unauthorised network traffic
before it leaves the internal network.
As unauthorised we consider all traffic that does \emph{not} originate from the
device's operating system or programs running on top of it, but e.g.\ from a
malicious network controller or an unwanted component that is soldered on to the
device's circuit board.

The final protocol design should\,\dots
\begin{itemize}
  \item have 128-bit security
  \item achieve a minimum throughput of \SI{500}{\Mbit\per\second} over a
  \SI{1}{\giga\bit\per\second} link on old hardware (e.g.\ a mid-2010 MacBook
  Pro)
\end{itemize}

Please refer to the \hyperref[sec:official-assignment]{official assignment} of
our supervisor in the appendix for the full description of the task given.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Glossary
\begingroup
  % Fix line breaks
  \setlength{\parfillskip}{1em plus 1fil}

  \glsaddall{glossary}  % print all terms
  \printterms[%
    database=glossary,
    columns=1,
    style=index,
    namefont={\bfseries},
    namecase=firstuc,% first letter of name upper case
    postdesc=none,
    prelocation=enspace,
    location=list
  ]
\endgroup

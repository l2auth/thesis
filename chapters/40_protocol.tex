% -*- mode: latex; coding: utf-8; TeX-master: ../thesis -*-
% !TEX TS-program = pdflatexmk
% !TEX encoding = UTF-8 Unicode
% !TEX root = ../thesis.tex

% -- (Beschreibt die Grundüberlegungen der realisierten Lösung
%    (Konstruktion/Entwurf) und die Realisierung als Simulation, als Prototyp
%    oder als Software-Komponente)
% -- (Definiert Messgrössen, beschreibt Mess- oder Versuchsaufbau,
%    beschreibt und dokumentiert Durchführung der Messungen/Versuche)
% -- (Experimente)
% -- (Lösungsweg)
% -- (Modell)
% -- (Tests und Validierung)
% -- (Theoretische Herleitung der Lösung)

As stated in the \hyperref[ch:introduction]{introduction}, our goal is to
prevent that packets injected into the network by an attacker get processed by
the recipient or routed by a gateway.

The problem with today's networking technologies is that it is easy for a
malicious actor to inject (unauthorised) packets into a network they have
physical access to, e.g.\ using spy chips soldered on to the circuit board by
the manufacturer or a service technician attaching a malicious device to the
network.

Thus it is not possible to trust packets' authenticity as anyone can inject
specially crafted packets into the network and use faked MAC or IP addresses to
circumvent firewalls.


\section{Attacker model}
\label{sec:attacker-model}

By an attacker we understand a malicious actor \emph{actively} trying to
exfiltrate sensitive data from the internal network using a device under his
control that is physically connected to the network.

An attacker could be a separate (unwanted) device connected to network, an
electronical component on a device's motherboard, malicious firmware in a piece
of the host's hardware, or potentially even malicious software using a
misconfigured operating system or hypervisor to send data into another network
segment using e.g.\ VLAN tagging.

We must assume that an attacker has a CPU, some sort of memory, and
full network access available to it and is not constrained by time.

While it is theoretically conceivable that a very advanced attacker
 could be able to gain access to the host's main memory, we believe that
this is very unlikely to be the case in the near future.
To gain access to the host's main memory, a spy chip would need quite a number
of traces on the motherboard routed to it for it to be able to read arbitrary
locations in the memory.
These would catch the eye of a keen observer.

Because we assume that the attacker is a separate hardware component, i.e.\ not
part of the host's CPU, and thus has no control over it, it cannot simply
use the host's operating system's networking stack to transmit data.
Instead, it must assemble correctly formatted Ethernet frames by itself and
either electronically or by using the \gls{NIC} send them off on the wire.
In any case, since the attacker will likely not have its own physical
connection, it must monitor the traffic and inject its Ethernet frames when
no one else is transmitting, otherwise there will be collisions which could be
detected by the host's operating system.

\subsubsection{Exclusions}

A passive attacker who simply collects statistics or private data that he has
seen on the link and then exfiltrates it using ways other than the
network---like on the next visit by a service technician---is considered out of
scope of this work.
Private data on the network should be protected using transport layer
security (TLS) or a VPN solution, which will prevent the attacker from
exfiltrating the data by stopping it from reading the sensitive data in the
first place.

Similarly, an attacker trying to \gls{DoS} the network by jamming the link or
degrading signal quality, thus forcing a reduction of the link speed, is
considered out of scope.

\section{Finding solutions}
\label{sec:finding-solutions}

The solution we envisage is to authenticate packets generated by the operating
system traversing the secured, internal network.
This way, all non-authenticated packets are considered malicious and silently
dropped by the first hop they traverse.

To be able to block all unauthenticated packets on the network, including
e.g.\ \gls{ARP} or \gls{ICMP}, it was clear to us that we have
to work on \gls{OSI} layer~2.

The original idea we had in mind when we started discussing this thesis was to
authenticate Ethernet frames using an HMAC.

\paragraph{What is an \gls{HMAC}?}
\label{par:what-is-hmac}

An \gls{HMAC} is a form of Message Authentication Code that uses a secret key in
combination with a cryptographic hash function and is defined in
RFC~2104\,\cite{rfc2104} as:
%
\begin{gather*}\label{eq:hmac-def}
  \text{HMAC}\left(K, m\right) = \text{H}\left(\left( K' \oplus \text{opad} \right) \|\
    \text{H}\left(\left( K' \oplus \text{ipad} \right) \|\ m \right)\right) \\
%
  K' =
  \begin{cases}
    \text{H}\left(K\right) & \text{K is larger than hash's block size} \\
    K & \text{otherwise}
  \end{cases}
\end{gather*}
with $H$ being a cryptographic hash function (e.g.\ \gls{SHA256}),\quad
$K$ being a secret key,\quad and $m$ being the message to authenticate.

$\text{opad}$ is the outer padding, consisting of $B$ repeated \texttt{0X5C} bytes. \\
$\text{ipad}$ is the inner padding, consisting of $B$ repeated \texttt{0x36} bytes. \\
$B$ is the block size of the cryptographic hash function $H$.


\subsection{Point-to-Bridge authentication}
\label{sec:p2p-auth}

The first solution we conceived was to authenticate Ethernet frames using an
\gls{HMAC} as they leave the sender's operating system and then check the frame
authentication on the bridge at the other end of the physical cable.  The bridge
would remove the frame authentication and forward the frame as if it were a
normal Ethernet frame.  However, frames from the bridge to the host are not
protected because---in contrast to frames sent by the host---it can be assumed
that if the frame were illegitimate, it would have already been dropped by a
bridge in the chain.

\begin{wrapfigure}{i}{.5\textwidth}
  \vspace{-1.5\baselineskip}
  \includegraphics[width=\linewidth]{original-idea}
  \caption{Point-to-Bridge authentication}
  \label{fig:p2p-auth}
  \vspace{-6\baselineskip}
\end{wrapfigure}

For this to work, both the bridge and the host need to share the same secret
key.  Because the authentication is only valid from the sending host to the
bridge the secret key could be different for each device connected to a single
bridge.

This solution is performant because authentication is only necessary on the
first hop (and only for sent packets).
Additionally, there is no infrastructure for key exchange needed as the key only needs
to be configured in two places.

One one hand, this solution already fulfils most of our desired security
properties:
\begin{enumerate}
  \item The rest of the network is protected from an affected host.
  \item{
    Each device, be it a client or a bridge, only needs to know exactly one
    key per connected wire.
    No sophisticated key exchange protocols are required.
  }
  \item{
    Malicious frames are filtered as soon as reach they the first bridge which
    limits the influence of a \gls{DoS} attack.
  }
  \item{
    Moreover, filtering of the frames on the first hop prevents that multiple
    attackers inside a single broadcast domain can communicate with each other.
  }
\end{enumerate}

On the other hand, there are three problems with this solution:
\begin{enumerate}
  \item{
    The bridge's software must be modified to check the frame authentication of
    all received frames and drop them if the authentication is invalid. \\
    But, because most bridges do not run a general-purpose operating system that
    can be easily modified and use \glspl{ASIC} for frame forwarding, making
    this solution work without breaking existing hardware would have been next
    to impossible.
  }
  \item{
    This solution fails to meet the required security properties if two hosts
    are directly connected to each other using a (cross-over) network cable
    without at least one bridge in between.
    This is because only bridges check the frames they receive, not the hosts.
  }
  \item{
    Reconfiguration of the bridge for each newly connected device is time
    consuming and error-prone.
  }
\end{enumerate}


\subsection{Public-key signatures}
\label{sec:sol-signed-frames}

As it became clear that the \textsl{\nameref{sec:p2p-auth}} approach has too many
downsides, we tried to improve upon it further.
The second design we came up with uses public-key cryptography to ensure
end-to-end trust; from the sender to the receiver or for communication with the
Internet: a host to the perimeter gateway.

We pursued the public-key approach hoping that we could keep the difficult to
modify bridges out of the loop while keeping the performance advantage of not
having to re-sign the frames on each hop.

\begin{figure}[H]
  \centering
  \includegraphics[width=.7\linewidth]{crypto-draft}
  \caption{Public-key signed frames: flow chart}
  \label{fig:pubkey-flowchart}
\end{figure}
\vspace{-\baselineskip}

When receiving a frame the device verifies that the signature of the frame is
valid using the sender's public key.
The signature verification works by calculating the hash of the frame and
comparing it to the signature attached to the frame, having been decrypted using
the sender's public key (cf.\ Figure~\ref{fig:pubkey-flowchart}).
Should both be equal, the frame's signature is valid.

To check the signature, every host on the whole network needs to be able to
know the public key of every other host it can communicate with.
To achieve this we conceived a key retrieval protocol.

\paragraph{Key retrieval} is somewhat similar to the ARP protocol.
To retrieve another public key the device sends out a broadcast message
(signed using its public key) asking for the public key of the other device's
IP address.
Any device that knows this public key generates a response message including
the desired public key. \\
When a gateway (a machine with \texttt{ip\_forward} enabled) receives such a
broadcast message, it is supposed to forward the request to other network
segments it is connected to, the same way it would forward an IP packet to the
host the public key is requested for.
If the gateway receives a response, it forwards the response to the original
requester.

As can be seen, a key retrieval can result in quite some network traffic.
To reduce the amount of network traffic for public key retrievals, every device
on the network has its own public key cache in which it stores previously
retrieved public keys.
The cache is not tied to a specific network interface, so on a gateway the cache
will eventually contain the public keys of all devices on the network and can be
seen as a distributed public key repository.

To ensure that the exchanged public keys are the correct ones, all public keys
in a network are signed by a common authority key pair. The authority's public
key is configured on all devices in the network and is used to verify the
received public key for a device.
The private key of the authority is to be kept secret and is only used to issue
new device keys.

While this solution might sound great at first, it has also some significant
downsides:

\begin{enumerate}
  \item{
    The public key distribution is complex and therefore bears a high risk of
    hidden vulnerabilities.
  }
  \item{
    Distributed key caches are required, adding more potential points of
    failure and thus making the network less robust against interruptions.
  }
  \item{
    Public-key cryptography is expensive (i.e.\ requires more CPU time than
    other solutions.) \\
    Moreover, the delivery of frames can be delayed if the receiver must first
    retrieve the sender's public key. \\
    In the worst case this could lead to a \gls{DoS} situation on the receiving
    host because it needs to retain all of the frames until the signature check
    has finished. If an attacker sent a lot of packets allegedly from hosts that
    do not exist, the receiver would need to run key discovery for each of them
    and retain them until the key discovery has finished, filling up the receive
    queue.
  }
  \item{
    Communication across the boundaries set by the OSI model
    (cf.\ Table~\ref{tab:osi-layers}) for layer 2 is needed for the key
    distribution to work, which goes against the principle of the OSI model.
  }
\end{enumerate}


\subsection{(Global) pre-shared key authentication}
\label{sec:global-psk}

After it has become clear that the \textsl{\nameref{sec:sol-signed-frames}}
solution cannot be the final solution to the problem, we decided to go back to
the initial \textsl{\nameref{sec:p2p-auth}} design and work on the
unsatisfactory parts. While improving the initial design we learned from the
mistakes of the overly complex public-key design and put emphasis on simplicity.

\begin{wrapfigure}{i}{.5\textwidth}
  % \vspace{-\baselineskip}
  \includegraphics[width=\linewidth]{global-psk}
  \caption{(Global) pre-shared key: network topology}
  \label{fig:global-psk-topology}
  \vspace{-2\baselineskip}
\end{wrapfigure}

We decided to toss out the signatures and use one pre-shared key that is known
to and used by every device within a broadcast domain.
This simplification, although it might reduce the configurative flexibility of
the user, makes the modification of the bridge's software a ``nice to have''
instead of a necessity.

In addition, network traffic is authenticated bidirectionally (both sent and
received frames have to be authenticated) as the original assumption---that the
bridge checks the frames' authentication---does not hold anymore. This also
allows for network setups which only involve two directly connected devices.

So let's say Alice wants to send an Ethernet frame to Bob, who happens to be in
the same broadcast domain as Alice is:
Alice compiles the Ethernet frame as she normally would and just before sending
it off on the wire, she calculates an \gls{HMAC} over the frame using the
pre-shared key and appends it to the frame.

When Bob receives the frame he will repeat the same procedure (but not include
the received \gls{HMAC} in his \gls{HMAC} calculation.) Should the \gls{HMAC} he
generated match the one Alice has sent in her frame, then the frame is correctly
authenticated and can be processed further
(cf.~Figure~\ref{fig:mac-verification}).
Should Bob's calculated \gls{HMAC} differ, he must not process the frame and
drop it.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.7\textwidth]{mac-verification}
  \caption[MAC verification]{MAC verification\fnm{}}
  \label{fig:mac-verification}
\end{figure}
\setcounter{footnotemarknum}{1}% ugly hack
\fnt{Source: \url{https://commons.wikimedia.org/wiki/File:Message_Authentication_Code.svg} [accessed 28.05.2019, style modified]}

Should Alice and Bob happen to be in different broadcast domains, the gateway
will first take Bob's role in the process, and after successful validation of
the \gls{HMAC} forward the packet to the destination, but this time use the
pre-shared key of the destination broadcast domain for authentication.

NB: In this design it would also be perfectly acceptable for a bridge to know
the pre-shared key, check the authentication of frames that pass through it, and
to drop the frames with invalid authentication.
But exfiltration of data will not work, even without support from the bridge or
if the packet is sent directly to the gateway, because the gateway will check
the frame's authentication and drop it if it is invalid.
Thus, illegitimate packets will under no circumstances leave the broadcast
domain.

\section{L2auth: the final protocol design}
\label{sec:protocol-design}

In agreement with out supervisor and not to break existing networking hardware,
we decided to extend the existing layer 2 Ethernet protocol in a way that looks
like an unknown layer 3 protocol to legacy networking hardware.

Working on layer 2 allows us to authenticate also non-IP packets, such as
\emph{ARP (Address Resolution Protocol)},
\emph{ICMP (Internet Control Message Protocol)}, or
\emph{VRRP (Virtual Router Redundancy Protocol)}.

NB: There is still data on the wire which cannot be controlled from within the
operating system, and as such cannot be authenticated. That is data generated
by components of the network itself, e.g.\ \emph{STP (Spanning Tree Protocol)}.

To wrap an Ethernet frame in our ``unknown'' lookalike layer 3 protocol, we do
three things: \\
Firstly, we replace the frame's \gls{ethertype} with one that is not used
by any other protocol for the time being. This is to ensure that the frame's
contents do not get processed by legacy networking hardware that would not
understand the modified contents. We chose \texttt{0x88e6} as the
L2auth \gls{ethertype}. \\
%
Secondly, we add the frame's original \gls{ethertype} to the head of the
payload, so that the receiver of the message is able to restore the original
\gls{ethertype} and knows what's inside. \\
%
Last but not least, we append a \emph{256-bit HMAC} (HMAC-SHA256) to the
end of the frame.

The structure of the modified datagram can be seen in
Figure~\ref{fig:l2auth-proto}.

It might seem a bit strange to add data to both the beginning and the end of the
frame's payload, but there is good reason to do so.
When calculating the HMAC of the frame it is important to ensure that the same
data is checked on the sender's and the receiver's side.
Since the sender cannot calculate an HMAC over the HMAC they are just trying to
calculate, it is important to ignore the data block that will contain the HMAC
when authenticating the frame.
Having the \gls{HMAC} at the end of the frame is beneficial as it simplifies the
code that needs to be written, because the \gls{HMAC} can be calculated over a
sequential buffer of data with no holes that need to be skipped.
%
Moreover, it would be possible to increase the transmission speed in simple
hardware, like bridges, because the \gls{HMAC} could be calculated while the
datagram is already being transmitted.

\vfil\pagebreak

\begin{figure}[H]
  \centering
  \begin{bytefield}[bitwidth=1.2em,endianness=little]{32}
    \bitheader{0,7,8,15,16,23,24,31} \\

    % Layer 2
    \bitbox[lrt]{32}{Preamble} \\
    \bitbox[lrb]{24}{} &
    \bitbox{8}{SFD\fnm{}} \\

    \bitbox[lrt]{32}{Destination \glslink{MAC-address}{MAC}} \\
    \bitbox[lrb]{16}{} &
    \bitbox[lrt]{16}{} \\
    \bitbox[lrb]{32}{Source \glslink{MAC-address}{MAC}} \\
    \bitbox{16}{\gls{ethertype} = 0x88E6} &

      % Layer 2.5
      \bitbox{16}{Real \gls{ethertype}} \\

        % Layer 3
        \wordbox[lrt]{2}{Data octets} \\
        \skippedwords \\
        \wordbox[lrb]{1}{} \\

      \wordbox{8}{\gls{HMAC} (HMAC-SHA256)} \\

    \bitbox{32}{Frame Check Sequence (CRC-32)} \\
  \end{bytefield}
  \caption{Final L2auth protocol design (complete authenticated Ethernet frame)}
  \label{fig:l2auth-proto}
\end{figure}
%
\fnt{Start of frame delimiter}

\textbf{Note:} The \textsl{Preamble}, \textsl{SFD}, and
\textsl{Frame Check Sequence} are part of Ethernet and not of L2Auth.
They are only included here for the sake of completeness.

\pagebreak

Packets arriving from an external network (like the Internet or legacy network
segments) will be authenticated unconditionally by the perimeter gateway before
they enter the internal network as there is no way to check their authenticity.
%
Similarly, for packets routed outside of the internal network the authentication
will be removed by the perimeter gateway before it forwards them to an external
network, as the authentication is meaningless to other networks.

\subsection{Security properties}
\label{sec:l2auth-security-properties}

For the sake of completeness we want to mention that, as already stated in the
\textsl{\nameref{sec:objective}}, L2auth only protects against unauthorised
transmission of datagrams by an attacker.  It is not our goal to stop an
attacker from seeing the transmitted data.

The security of the L2auth protocol is primarily defined by the \gls{HMAC}
function:
\begin{quote}
  The security of the [HMAC] depends on cryptographic properties of the hash
  function $H$: the resistance to collision finding (limited to the case where
  the initial value is secret and random, and where the output of the function
  is not explicitly available to the attacker), and the message authentication
  property of the compression function of $H$ when applied to single blocks (in
  HMAC these blocks are partially unknown to an attacker as they contain the
  result of the inner $H$ computation and, in particular, cannot be fully chosen
  by the attacker).  \signed{RFC~2104\,\cite{rfc2104}}
\end{quote}

In the case of L2auth, the hash function $H$ is \gls{SHA256} which provides
\SI{128}{\bit{}s} of security against a classical collision attack.

In addition to a strong cryptographic hash function, L2auth follows the
\gls{HMAC} key length recommendations of RFC~2104\,\cite{rfc2104} and enforces
the pre-shared key to be at least $L$\,bytes (in the case of \gls{SHA256} \SI{32}{bytes})
long.  By enforcing the key length we cannot only assure optimal cryptographic
performance of the \gls{HMAC} function, we also explicitly declare the key as
binary to push users into the right direction of generating random keys.

\textbf{Note:} To generate such a secure, random key the following command line
can be used: \\
% \lstinline[language=sh]!od -An -v -tx -N 32 </dev/random | tr -d '\n' | sed 's/ //g'! \\
\lstinline[language=sh]!hexdump -v -e '"%08x"' -n 32 </dev/random! \\

The L2auth protocol does not offer end-to-end trust or encryption, forward
secrecy, or future secrecy.
Neither does L2auth provide replay protection, as repeated frames are to be
expected on layer~2. If replay protection is necessary, there exist upper layer
protocols like TCP which provide that.

\paragraph{Limitations}

We are aware that this solution has three limitations:
\begin{enumerate}
  \item{
    If a trusted device in the network (one that knows the
    pre-shared key) can be tricked into authenticating untrustworthy datagrams,
    all other devices will also accept them as valid.
  }
  \item{
    Once a perimeter gateway is infected, a complete protection
    against exfiltration of data cannot be guaranteed as malicious chips in the
    \gls{NIC} could route packets underhandedly past the respective host's
    operating system.
    In other words, at least one device in the chain must be trustworthy so
    that protection can be guaranteed.
  }
  \item{
    Because packets from external networks will be authenticated
    unconditionally by the gateway that routes them into the internal network,
    authenticity and integrity of those packets before entering the internal
    network cannot be guaranteed. We suggest transport layer security (TLS) for
    that purpose.
  }
\end{enumerate}


\subsection{Key rotation}
\label{sec:l2auth-key-rotation}

In order to reduce the impact of the missing forward and future secrecy in the
protocol and to allow for replacement of compromised keys without downtime, each
network client must be able to accept frames authenticated using at least two
keys at the same time.
A frame is valid if the authentication is valid for any of the keys.

L2auth does not provide a provision for key rotation itself. It has been
designed so that key rotation can be accomplished using means already present in
the respective setup, preferably a configuration management system or
alternatively SSH\footnote{For efficient management of large setups tooling like
  \href{https://github.com/chaos/pdsh}{pdsh} or
  \href{https://parallel-ssh.org}{parallel-ssh} can be used.}.

Key rotation makes use of the support for multiple keys (we use two keys called
\emph{primary key} and \emph{backup key}).

\vfil\pagebreak
It works as follows:
\begin{enumerate}
  \item A new key for the network segment is generated.
  \item{
    All devices (incl. bridges if they support L2auth) are reconfigured to use
    the newly generated key as their \emph{backup key}.
  }
  \item{
    The clients are reconfigured using the new key.
    The old key gets moved into the \emph{backup key} slot to ensure that
    connections with clients that haven't been reconfigured yet will continue to
    work without interruption.
  }
  \item{
    After all clients have been reconfigured, the new key is set as the
    \emph{primary key} on gateways and bridges.
    The \emph{backup key} can be cleared.
  }
  \item Finally, the \emph{backup key} slot is cleared on all remaining devices.
\end{enumerate}

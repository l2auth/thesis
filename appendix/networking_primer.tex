% -*- mode: latex; coding: utf-8; TeX-master: ../thesis -*-
% !TEX TS-program = pdflatexmk
% !TEX encoding = UTF-8 Unicode
% !TEX root = ../thesis.tex

The ISO\footnote{International Organization for Standardization (ISO).\\Note: Because its name would have different acronyms in different languages (e.g.\ IOS in English, OIN in French for \textsl{Organisation internationale de normalisation}), the name ISO has been given. ISO is derived from the Greek isos, meaning equal.} has
conceived a model for communication between computer systems (``Open Systems''),
the so called \textsl{Open Systems Interconnection (OSI)} model, which has
been adopted by all prevalent networking systems.

\begin{figure}[H]
  \centering
  \includegraphics[width=.8\linewidth]{1-osi-layer-ref}
  \caption[Seven layer reference model and peer protocols]{%
    Seven layer reference model and peer protocols\,%
    \cite[p.~28]{ISO7498-1}}
  \label{fig:osi-communication-relay}
\end{figure}

It consists of 7 layers (cf.~Figure~\ref{fig:osi-communication-relay}) and
ranges from the physical layer (layer 1) all the way to the application
layer (layer 7).

In the way the OSI model was designed, there is a strong separation between the
different layers.  Each layer provides a defined interface only to the layer
directly below and above it.
Communication only happens on the same layer. A sender sending data on layer~3
cannot address it to a recipient on e.g.\ layer~2.

When an application wants to send data (a payload) it packs it up on layer 7 and
sends the result down to a protocol handler of its choice on layer 6, which in
turn wraps it again and sends the result down to layer 5, and so on.  When the
data reaches layer 1, the network controller will send it out on the wire.  The
wire can be seen as layer 0, if you will.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{1-x200-mapping-between-layers}
  \caption[An illustration of mapping between data-units in adjacent layers]{%
    An illustration of mapping between data-units in adjacent layers
    \cite[p.~16]{ISO7498-1}}
  \label{fig:x200-mapping-between-layers}
\end{figure}

On the receiver's side the same process occurs in reverse (layer 1 receives the
data from the wire and passes it to layer 2, which unpacks it and passes the
inner data on to layer 3, and so forth).

\begin{figure}[H]
  \includegraphics[width=\textwidth]{1-osi-onion}
  \caption{Parts of a datagram separated by OSI layer}
  \label{fig:osi-onion}
  \vspace{-\baselineskip}
\end{figure}
\pagebreak

% One of the strongest advantages of the Internet is the fact that different
% network segments can be connected together using relay stations (more
% commonly called: ``routers'').
% %
% To fulfil their job, relay stations only need to be able to understand
% protocols up to the ``network layer''
% (cf.~Figure~\ref{fig:osi-communication-relay}).

As can be seen in Figure~\ref{fig:osi-onion}, each additional layer adds more
addressing information, thus enlarging the possible reach of the packet.
%
In table~\ref{tab:osi-layers} you can find an overview of the maximum possible
reach of a packet when only the information up to a certain layer is considered.

% https://en.wikipedia.org/wiki/List_of_network_protocols_(OSI_model)#Layer_2.5
\begin{table}[H]
  \centering
  \begin{tabularx}{\textwidth}{llll}
    \toprule[1.5pt]
    \thead{Layer} & \thead{Reach} & \thead{Addressing} & \thead{Examples} \\
    \midrule

    1 Physical layer & cable & \textit{none} & Manchester enc. \\
    2 Data link layer & \gls{broadcast-domain} & MAC address & Ethernet, Wi-Fi \\
    3 Network layer & hosts on the Internet & IP address & IP, ICMP \\
    4 Transport layer & service on a host & IP address + port & TCP, UDP \\

    \bottomrule[1.5pt]
  \end{tabularx}
  \caption{Common layers of the OSI model}
  \label{tab:osi-layers}
\end{table}

The OSI model is a very theoretical concept. Today's implementations do not
adhere strictly to it. The session and presentation layers are mostly unused
these days and in implementations there are usually no clear boundaries between
the different layers.
Nevertheless, it is still very useful to know the idea of the OSI model to
understand how and why the different parts of the system work together.

For further information on this topic we recommend the book
\emph{TCP/IP Illustrated (Volume~1)}\,\cite{Stevens:2011:TIP}.

\vfil
